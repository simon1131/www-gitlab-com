---
title: "Why We Use Personas in Product Development"
author: Sarah O’Donnell
author_twitter: saraheod
categories: technical overview
image_title: '/images/blogimages/the-importance-of-ux-personas.jpg'
description: 'Our User Experience (UX) Researcher explains what personas are and how they change the way teams work'
---

<p class="alert alert-orange" style="background-color: rgba(252,163,38,.3); border-color: rgba(252,163,38,.3); color: rgb(226,67,41) !important; text-align: center;">Catch our upcoming webcast &nbsp;&nbsp;<i class="fa fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> &nbsp;&nbsp;<strong>Designing For the Modern Developer</strong> &nbsp;&nbsp;<i class="fa fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
&nbsp;&nbsp;on January 19. <a style="color: rgb(107,79,187);" href="https://page.gitlab.com/uxlivestream_landingpage.html?utm_medium=blog&utm_source=blog&utm_campaign=ux+webcast&utm_content=register+now+button">Register here</a>!</p>

When developing a product, it’s easy to assume that users are just like ourselves. If we rely on our assumptions to lead what and how we develop, there is a risk of creating a product that may only work for a segment of users. A more analytical approach is necessary for understanding the needs of all users, which is paramount to successful user experience (UX). 

<!--more-->

Every month, we piece together the jigsaw of new features and design tweaks, and all the tools that ship with GitLab, ensuring that each change fits within our vision and understanding of user needs. To help with this task, we're [developing](https://www.surveymonkey.co.uk/r/GitLab) personas that will allow us to relate to different types of users and subsequently predict their behavior. This is essential for our key challenge of creating a unified experience for teams big and small, so they can stay focused on their own goals. In this post, I'll explore some of the reasons to use personas in product development. 

## What is a persona?

Personas are fictional characters created to represent the major needs and expectations of the different types of users that use a website, product or service. A persona typically has a name, a picture, and a background, along with demographic information such as age, highest level of education and work experience. 

Imagine the last time you were engrossed in a television series. It’s likely that you connected with one or more of the leading characters because you could understand - and on some level, relate to - their emotions, responses and actions in a given situation. You probably discussed the television series with a friend and together, you may have guessed at future plot lines based on the events that had occurred so far in the series. Personas work in a similar way.

## What can personas reveal? 

 A good persona generates empathy for users by putting a “human face” on data. Its aim is to summarize and share research findings with anybody contributing towards the success of the product. It’s easier for an individual (regardless of their job title) to understand a persona that collectively represents the motivations, frustrations and goals of thousands of users, than it is to trawl through days, or even months' worth, of research findings. 

 Personas provide a clear understanding of why and how a person is using your product, and documents any pain points they experience, both in the context of using the product and the environment in which the product is used. Finally, it summarizes their goals: what do they hope to solve or achieve by using your product? This information is normally collected by using both qualitative (surveys, user interviews, etc.) and quantitative research techniques (web analytics). While a persona itself is fictional, it is formed using factual data to provide a realistic model of user needs.

## How do personas change the way teams work?

 Personas promote further discussions within a team about how a user would interact with a proposed idea. By internalizing the user, we adopt their mentality and form solutions based on what’s best for the user and their given situation.

 By defining personas, there is a well-documented, clear focus on who the product is for, which stops users’ needs from being altered to suit an idea or concept. A product manager may use personas to validate and prioritize features, whereas a designer may use them to determine the overall visual style of a product. When everybody within a team has a shared understanding of users, disagreements surrounding product development are reduced, as there is greater consensus about what is right for users. 

## What is the state of personas at GitLab?

At GitLab, we are in the early stages of discovering who our personas are. If you want to make sure that your needs and expectations as a user are met, then you can help us by [completing our survey][survey link] and sharing your views. 

The qualitative data from this survey will be analyzed and categorized by coding similar responses. A code is a word or short phrase that describes a respondent’s answer, and serves to condense the information collected into key themes and topics. We'll use statistical analysis to summarize and describe the quantitative data (close-ended questions) in the survey, and we can interpret the collective findings to form a basis for our personas. 

Because it has some limitations, we interpret survey data alongside user interviews and web analytics. This helps us compare findings and make the strongest conclusions possible. We're looking forward to learning about how you use GitLab!


_Tweet [@GitLab](https://twitter.com/gitlab) and check out our [job openings](https://about.gitlab.com/jobs/)._

Image: "[Curves](https://www.flickr.com/photos/sidneiensis/14109676698/in/photolist-nuPMiU-ryzBme-aBf95E-bhuWaX-dWMhVA-9hrsBU-cwgKsS-dWFxBg-6HobWf-9s5y2P-fuV8He-eAEQoD-fvRBo6-ftDY1D-97v8g5-MxynM-3fawkY-nuJMy8-97s9px-fuVaex-qYfefX-bAojmQ-fyrNcH-aCR5c2-7XA7iP-cyqD8N-49HGS-8oVQhu-pt2tn1-74753h-2zT9w3-7PqwNc-7476K7-dN1rGL-fsXKRX-kUua-746WoE-8fabaP-oJPHDc-a6TGaF-eDSoXL-5bJjta-g6njp8-ftDXdV-8XKrHW-g2H8EV-dMUTPp-9s5xpn-ftd733-brt87D)" by [Jason Tong](https://www.flickr.com/photos/sidneiensis/) is licensed under [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/legalcode).

<!-- Identifiers, in alphabetical order -->

[survey link]: https://www.surveymonkey.co.uk/r/GitLab
